import { useState } from 'react'
import TaskEntry from './TaskEntry'
import { useSelector } from 'react-redux'
import { Task, selectTasks } from './tasksSlice'


interface Props { }

function TaskList({ }: Props) {
    const tasks = useSelector(selectTasks)
    const [filter, setFilter] = useState<string | null>(null)

    const filterFn = (o: Task): Boolean => {
        if (filter == "complete")
            return o.completed == true

        if (filter == "incomplete")
            return o.completed == false

        return true
    }

    return (
        <>
            <div className={"flex justify-end gap-1 text-xs"}>
                <div className={"cursor-pointer"}>
                    <span
                        className={`select-none p-1 px-2 rounded-full transition-colors ${filter == "complete" && 'bg-blue-600 text-white'}`}
                        onClick={_ => { setFilter(f => f == "complete" ? null : "complete") }}>Выполненные</span>
                </div>
                <div className={"cursor-pointer"}>
                    <span
                        className={`select-none p-1 px-2 rounded-full transition-colors ${filter == "incomplete" && 'bg-blue-600 text-white'}`}
                        onClick={_ => { setFilter(f => f == "incomplete" ? null : "incomplete") }}>Невыполненные</span>
                </div>
            </div >
            <ul
                className={"w-full border-solid border-2 rounded-md flex flex-col-reverse p-2 gap-1"}>
                {tasks.length > 0 ? <> {
                    tasks.slice().filter(filterFn).map(t => {
                        return (
                            <TaskEntry key={t.id} id={t.id} text={t.text} completed={t.completed} />
                        )
                    })
                }
                </> : (<span className={"text-gray-400"}>Для начала, добавьте задачу</span>)}
            </ul>
        </>
    )
}

export default TaskList