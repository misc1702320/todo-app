import { PayloadAction, createSlice } from "@reduxjs/toolkit";
import { RootState } from "../../app/store";


//Task type
export interface Task {
    id: number,
    text: string,
    completed: boolean
}

//Slice state type
export interface TasksState {
    value: Task[]
}

const initialState: TasksState = {
    value: []
}


export const tasksSlice = createSlice({
    name: "tasks",
    initialState,
    reducers: {
        addTask: (state, action) => {
            const newTask = {
                id: Date.now(),
                text: action.payload,
                completed: false
            };
            state.value.push(newTask)
        },
        editTask: (state, action: PayloadAction<{ id: number, text: string }>) => {
            const task = state.value.find(o => o.id == action.payload.id)
            if (task) {
                task.text = action.payload.text
            }
        },
        removeTask: (state, action: PayloadAction<number>) => {
            const i = state.value.findIndex(o => o.id == action.payload)
            if (i !== -1) {
                state.value.splice(i, 1)
            }
        },
        toggleTaskStatus: (state, action: PayloadAction<number>) => {
            const task = state.value.find(o => o.id == action.payload)
            if (task) {
                task.completed = !task.completed
            }
        }
    }
})

export const { addTask, removeTask, toggleTaskStatus, editTask } = tasksSlice.actions
export const selectTasks = (state: RootState) => state.tasks.value
export default tasksSlice.reducer