import { useState } from 'react'

import { addTask } from './tasksSlice'
import { useDispatch } from 'react-redux'
import AutoTextarea from './AutoTextarea'

interface Props { }

function TaskCreator({ }: Props) {

    const [text, setText] = useState("")
    const dispatch = useDispatch();

    const makeTask = () => {
        if (text.length == 0)
            return

        dispatch(addTask(text))
        setText("")
    }

    return (
        <div className={"w-full flex resize-none border-solid border-2 rounded-md"}>
            <AutoTextarea value={text} onTextChange={(t) => setText(t)} />
            <div className={"w-px border-solid border-e my-2 mx min-w-2"}></div>
            <button onClick={_ => makeTask()} className='self-start bg-blue-600 text-white rounded-md text-sm font-bold p-2 m-1 leading-none'>{"+"}</button>
        </div>
    )
}

export default TaskCreator