import React, { useEffect, useImperativeHandle, useRef, useState } from 'react'

interface AutoTextAreaProps {
    onTextChange: (t: string) => void
    defaultValue?: string
    value?: string
}

const AutoTextarea = React.forwardRef<HTMLTextAreaElement, AutoTextAreaProps>(({ defaultValue, value, onTextChange }, ref) => {

    const innerRef = useRef<HTMLTextAreaElement>(null);
    useImperativeHandle(ref, () => innerRef.current as HTMLTextAreaElement);

    const [text, setText] = useState("")

    useEffect(() => {
        if (innerRef.current) {

            innerRef.current.style.height = '0px'
            innerRef.current.style.height = innerRef.current.scrollHeight + "px"
        }
    }, [text, value])

    return (
        <textarea
            ref={innerRef}
            defaultValue={defaultValue}
            value={value}
            onChange={e => { setText(e.currentTarget.value); onTextChange(e.currentTarget.value) }}
            className="w-full resize-none whitespace-pre-wrap h-auto p-2" />
    )
})

export default AutoTextarea