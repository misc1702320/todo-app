import { useCallback, useEffect, useRef, useState } from 'react'
import { useDispatch } from 'react-redux'
import { editTask, removeTask, toggleTaskStatus } from './tasksSlice'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCheck, faPencil, faTrash } from '@fortawesome/free-solid-svg-icons'
import AutoTextarea from './AutoTextarea'

interface Props {
    id: number,
    text: string,
    completed: boolean
}

function TaskEntry({ id, text, completed }: Props) {

    const dispatch = useDispatch()

    const editedText = useRef("")
    const [editing, setEditing] = useState(false)

    useEffect(() => {
        editedText.current = text
    }, [])

    const textareaRef = useCallback((node: HTMLTextAreaElement) => {
        if (node) {
            node.focus()
            node.setSelectionRange(node.value.length, node.value.length);
        }
        return node
    }, [])

    const getDate = () => {
        const dt = new Date(id)
        return dt.getDate() + "/" + (dt.getMonth() + 1) + "/" + dt.getFullYear() + " | " + dt.getHours() + ":" + dt.getMinutes().toString().padStart(2, "0");
    }

    return (
        <li
            className={`border-solid border-2  rounded-md p-2 flex transition-colors
        ${completed ? "bg-green-300 border-green-500" : "bg-white"}`}>
            <div className={"w-full flex flex-col"}>
                <div className={"flex gap-2 align-middle mb-1"}>
                    <span className={`text-xs transition-colors ${completed ? "text-green-600" : "text-gray-400"}`}>
                        {getDate()}
                    </span>
                </div>
                <hr className={`transition-colors ${completed ? "bg-green-300 border-green-500" : ""}`}></hr>
                {editing ? <AutoTextarea ref={textareaRef} defaultValue={text} onTextChange={t => editedText.current = t} /> : <p className={"p-2 whitespace-pre-wrap"}>{text}</p>}

            </div>
            <div className={`w-px border-solid border-r mx-2 transition-colors ${completed ? "bg-green-300 border-green-500" : ""}`}></div>
            <div className={"flex flex-col gap-2"}>
                <div className={"relative flex justify-center align-middle"}>
                    <input
                        className={"aspect-square w-[24px] transition-colors  cursor-pointer appearance-none border-2 border-solid rounded-sm checked:bg-green-600 checked:border-0"}
                        type='checkbox'
                        defaultChecked={completed}
                        onClick={_ => dispatch(toggleTaskStatus(id))} />
                    {completed && <FontAwesomeIcon className={"text-white absolute top-1 pointer-events-none"} icon={faCheck} />}
                </div>


                <button
                    className={"bg-transparent aspect-square w-[24px] transition-colors rounded-sm hover:bg-slate-400 hover:bg-opacity-25"}
                    onClick={_ => {
                        if (editing) {
                            dispatch(editTask({ id: id, text: editedText.current }))
                        }
                        setEditing(e => !e)
                    }}
                ><FontAwesomeIcon icon={editing ? faCheck : faPencil} /></button>
                <hr className={`transition-colors ${completed ? "bg-green-300 border-green-500" : ""}`}></hr>
                <button
                    className={"bg-transparent aspect-square w-[24px] transition-colors rounded-sm hover:bg-red-400 hover:bg-opacity-25"}
                    onClick={_ => dispatch(removeTask(id))}>
                    <FontAwesomeIcon icon={faTrash} className={"text-red-500"} />
                </button>
            </div>
        </li>
    )
}

export default TaskEntry