import { configureStore, createListenerMiddleware, isAnyOf } from "@reduxjs/toolkit";
import taskReducer, { addTask, editTask, removeTask, toggleTaskStatus } from '../features/tasks/tasksSlice'

export const listenerMiddleware = createListenerMiddleware();
listenerMiddleware.startListening({
    matcher: isAnyOf(addTask, removeTask, editTask, toggleTaskStatus),
    effect: (_, listenerApi) => {
        localStorage.setItem("tasks", JSON.stringify((listenerApi.getState() as RootState).tasks))
    }
})

const tasksState = JSON.parse(localStorage.getItem('tasks') || "null")
export const store = configureStore({
    preloadedState: {
        tasks: tasksState === null ? { value: [] } : tasksState
    },
    reducer: {
        tasks: taskReducer,
    },
    middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(listenerMiddleware.middleware)
})

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch