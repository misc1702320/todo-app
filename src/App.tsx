import './App.css'
import TaskList from './features/tasks/TaskList'
import TaskCreator from './features/tasks/TaskCreator'

function App() {
  return (
    <main className={"min-h-full flex flex-col flex-1 justify-center gap-2 md:max-w-80"}>
      <h1 className="text-3xl font-bold text-center mb-2">
        To-Do App
      </h1>
      <TaskCreator/>
      <TaskList></TaskList>
    </main>
  )
}

export default App
